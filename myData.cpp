#include <string>
#include <iostream>
#include "myData.h"
using namespace std;

myData::myData(pthread_mutex_t* mut, pthread_cond_t* cond) {
    this->nazovSuboru = "";
    this->domena = "";
    this->adresa = "";
    this->token = "";
    this->meno = "";
    this->heslo = "";
    this->hod=0;
    this->min=0;
    this->casovanie=false;
    this->stop = 0;
    this->paused = 0;
    this->mutexStahovanie = mut;
    this->resume = cond;
}

int myData::rozdelUrlAdresu(string urlAdresa) {
    if(urlAdresa.find("//") != string::npos) {
        this->token = urlAdresa.substr(0, urlAdresa.find("//"));
    } else {
        return 0;
    }
    string withoutToken = urlAdresa.substr(token.length() + 2, urlAdresa.length());

    if(withoutToken.find("/") != string::npos) {
        string someString = withoutToken.substr(0, withoutToken.find("/"));

        this->domena = const_cast<char *>(someString.c_str());
        this->adresa = withoutToken.substr(someString.length() + 1, withoutToken.length());
        return 1;
    }  else {
        return 0;
    }
}

int myData::nacitajData() {
    string url;
    string nazov;

    printf("Zadajte adresu: ");
    cin >> url;
    rozdelUrlAdresu(url);
    if(rozdelUrlAdresu(url)) {
        if (this->token == "sftp:" || this->token == "ftp:") {
            cout << "Zadajte meno do serveru: ";
            cin >> meno;
            cout << "Zadajte heslo na server: ";
            cin >> heslo;
        } else if (this->token == "http:" || this->token == "https:") {

        }

        printf("Zadajte názov pre stiahnuty subor: ");
        cin >> nazov;

        this->nazovSuboru =&nazov[0];
    } else {
        cout << "Webova adresa je neplatná\n";
        return 0;
    }
}

string myData::getToken() {
    return this->token;
}
