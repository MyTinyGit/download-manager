//
// Created by Dominik-PC on 10/01/2021.
//

#ifndef DOWNLOADMANAGER_DOWNLOADSFTP_H
#define DOWNLOADMANAGER_DOWNLOADSFTP_H
class downloadSFTP {
public:
    downloadSFTP(int a);
    int download(myData* dataz);
    static int waitsocket(int socket_fd, LIBSSH2_SESSION *session);
};

#endif //DOWNLOADMANAGER_DOWNLOADSFTP_H
