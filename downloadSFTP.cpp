#include <iostream>
#include <libssh2.h>
#include <libssh2_sftp.h>

#include <string>
#include <ctime>
#include <unistd.h>
#include <stdlib.h>
#include <netdb.h>
#include "downloadSFTP.h"

using namespace std;

int downloadSFTP::waitsocket(int socket_fd, LIBSSH2_SESSION *session)
{
    struct timeval timeout;
    int rc;
    fd_set fd;
    fd_set *writefd = NULL;
    fd_set *readfd = NULL;
    int dir;

    timeout.tv_sec = 10;
    timeout.tv_usec = 0;

    FD_ZERO(&fd);

    FD_SET(socket_fd, &fd);

    /* now make sure we wait in the correct direction */
    dir = libssh2_session_block_directions(session);

    if(dir & LIBSSH2_SESSION_BLOCK_INBOUND)
        readfd = &fd;

    if(dir & LIBSSH2_SESSION_BLOCK_OUTBOUND)
        writefd = &fd;

    rc = select(socket_fd + 1, readfd, writefd, NULL, &timeout);

    return rc;
}

downloadSFTP::downloadSFTP(int a) {

}

int downloadSFTP::download(myData* dataz) {
    int sock, auth_pw = 1;
    struct sockaddr_in sin;
    LIBSSH2_SESSION *session;

    cout << "\n";
    string sftppath = "/" + dataz->adresa; //Cesta odkial

    int rc;
    LIBSSH2_SFTP *sftp_session;
    LIBSSH2_SFTP_HANDLE *sftp_handle;
    FILE *tempstorage;
    char mem[2];
    struct timeval timeout;
    fd_set fd;
    fd_set fd2;

    struct hostent *server;
    server = gethostbyname(dataz->domena.c_str());
    rc = libssh2_init(0);
    if(rc != 0) {
        fprintf(stderr, "libssh2 initialization failed (%d)\n", rc);
        dataz->stop = 1;
        return 0;
    }

    sock = socket(AF_INET, SOCK_STREAM, 0);

    sin.sin_family = AF_INET;
    sin.sin_port = htons(10022);
    sin.sin_addr = *((struct in_addr *)server->h_addr);
    if(connect(sock, (struct sockaddr*)(&sin),
               sizeof(struct sockaddr_in)) != 0) {
        fprintf(stderr, "failed to connect!\n");
        dataz->stop = 1;
        return 0;
    }

    session = libssh2_session_init();
    if(!session)

        libssh2_session_set_blocking(session, 1);

    rc = libssh2_session_handshake(session, sock);

    if(rc) {
        fprintf(stderr, "Failure establishing SSH session: %d\n", rc);
        dataz->stop = 1;
        return 0;
    }


    if(auth_pw) {
        while((rc = libssh2_userauth_password(session, dataz->meno.c_str(), dataz->heslo.c_str()))
              == LIBSSH2_ERROR_EAGAIN);
        if(rc) {
            fprintf(stderr, "Authentication by password failed.\n");
            dataz->stop = 1;
            return 0;
        }
    }

    do {
        sftp_session = libssh2_sftp_init(session);


        if(!sftp_session) {
            if(libssh2_session_last_errno(session) ==

               LIBSSH2_ERROR_EAGAIN) {
                fprintf(stderr, "non-blocking init\n");
                waitsocket(sock, session);
            }
            else {
                fprintf(stderr, "Unable to init SFTP session\n");
                dataz->stop = 1;
                return 0;
            }
        }
    } while(!sftp_session);


    do {
        sftp_handle = libssh2_sftp_open(sftp_session, sftppath.c_str(),

                                        LIBSSH2_FXF_READ, 0);

        if(!sftp_handle) {
            if(libssh2_session_last_errno(session) != LIBSSH2_ERROR_EAGAIN) {
                fprintf(stderr, "Unable to open file with SFTP\n");
                dataz->stop = 1;
                return 0;
            }
            else {
                fprintf(stderr, "non-blocking open\n");
                waitsocket(sock, session); /* now we wait */
            }
        }
    } while(!sftp_handle);

    string user = getenv("USER");
    string toOpen = "/home/"+ user + "/DownloadManager/" + dataz->nazovSuboru;
    tempstorage = fopen(toOpen.c_str(), "wb");

    do {
        do {
            rc = libssh2_sftp_read(sftp_handle, mem, sizeof(mem));

            while(dataz->paused == 1) {
                pthread_cond_wait(dataz->resume, dataz->mutexStahovanie);
            }

            if(rc > 0) {
                fwrite(mem, rc, 1, tempstorage);
                cout << "Stahujem" << rc << endl;
                sleep(2);
            }

        } while(rc > 0);

        if(rc != LIBSSH2_ERROR_EAGAIN) {
            break;
        }
        timeout.tv_sec = 10;
        timeout.tv_usec = 0;

        FD_ZERO(&fd);
        FD_ZERO(&fd2);
        FD_SET(sock, &fd);
        FD_SET(sock, &fd2);

        rc = select(sock + 1, &fd, &fd2, NULL, &timeout);
        if(rc <= 0) {
            fprintf(stderr, "SFTP download timed out: %d\n", rc);
            dataz->stop = 0;
            return 0;
        }

    } while(1);

    dataz->stop = 1;

    libssh2_sftp_close(sftp_handle);
    libssh2_session_disconnect(session, "Normal Shutdown");

    libssh2_session_free(session);
    fclose(tempstorage);

    libssh2_exit();
    close(sock);
}
