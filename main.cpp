
#include <string>
#include <ctime>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

#include "myData.cpp"
#include "myData.h"
#include "downloadHttp.cpp"
#include "downloadFtp.cpp"
#include "downloadSFTP.cpp"

using namespace std;

void pracaSoSuborom();

void* sftpVlakno(void* args) {
    myData * dataz = (myData *)args;
    downloadSFTP downloader(0);

    if(dataz->casovanie) {
        int pom = 0;
        while (pom == 0) {
            time_t t = time(0);
            tm *now = localtime(&t);
            if (dataz->hod == now->tm_hour && dataz->min == now->tm_min) {
                dataz->casovanie = false;
                downloader.download(dataz);
                pom = 1;
                break;
            }
        }
    } else {
        downloader.download(dataz);
    }
    return NULL;
}

void * ftpVlakno(void* args) {
    myData * dataz = (myData *)args;
    downloadFtp downloader(0);

    if(dataz->casovanie) {
        int pom = 0;
        while (pom == 0) {
            time_t t = time(0);
            tm *now = localtime(&t);
            if (dataz->hod == now->tm_hour && dataz->min == now->tm_min) {
                dataz->casovanie = false;
                downloader.download(dataz);
                pom = 1;
                break;
            }
        }
    } else {
        downloader.download(dataz);
    }
}

void * httpsVlakno(void * args) {
    myData * dataz = (myData *)args;
    downloadHttp downloader(0);

    if(dataz->casovanie) {
        int pom=0;
        while(pom==0){
            time_t t = time(0);
            tm *now = localtime(&t);
            if (dataz->hod == now->tm_hour && dataz->min == now->tm_min) {
                dataz->casovanie = false;
                downloader.download(dataz);
                pom = 1;
                break;
            }
        }
    } else {
        downloader.download(dataz);
    }
}

void * hlavneVlakno(void * args) {
    system("mkdir /home/${USER}/DownloadManager");
    myData * dataz = (myData *)args;
    pthread_t download;

    int running = 1;
    int stahuje = 0;

    while(running > 0) {
        cout << "Zvoľte jednu z možností:" << endl;
        cout << "   1 pre stiahnutie" << endl;
        cout << "   2 pre správu adresárov" << endl;
        cout << "   3 pre načasovanie sťahovania" << endl;
        cout << "   0 pre exit" << endl;
        cin >> running;

        while(cin.fail()) {
            cout << "Zadajte prosim cislo" << std::endl;
            cin.clear();
            cin.ignore(256,'\n');
            cin >> running;
        }

        switch(running) {
            case 1: {
                dataz->nacitajData();
                if (dataz->token == "https:" || dataz->token == "http:") {
                    pthread_create(&download, NULL, &httpsVlakno, dataz);
                    stahuje = 1;
                } else if (dataz->token == "ftp:") {
                    pthread_create(&download, NULL, &ftpVlakno, dataz);
                    stahuje = 1;
                } else if (dataz->token == "sftp:") {
                    pthread_create(&download, NULL, &sftpVlakno, dataz);
                    stahuje = 1;
                }
                break;
            }

            case 2: {
                pracaSoSuborom();
                break;
            }

            case 3: {
                char ch;
                int hodina, minuta;
                cout << "Zadajte čas kedy sa má sťahovať v tvare: HH:MM"<<endl;
                cin >> hodina>>ch >> minuta;
                dataz->hod = hodina;
                dataz->min =  minuta;
                dataz->casovanie=true;

                dataz->nacitajData();
                if (dataz->token == "https:" || dataz->token == "http:") {
                    pthread_create(&download, NULL, &httpsVlakno, dataz);
                } else if (dataz->token == "ftp:") {
                    pthread_create(&download, NULL, &ftpVlakno, dataz);
                } else if (dataz->token == "sftp:") {
                    pthread_create(&download, NULL, &sftpVlakno, dataz);
                }
                break;
            }
            case 0: {
                return 0;
            }
        }

        while(dataz->casovanie) {
            cout << "Zvoľte jednu z možností:" << endl;
            cout << "   1 pre spravu vasho buduceho stahovania" << endl;
            cout << "   2 pre správu adresárov" << endl;
            cout << "   0 pre exit" << endl;
            cin >> running;

            while(cin.fail()) {
                cout << "Zadajte prosim cislo" << endl;
                cin.clear();
                cin.ignore(256,'\n');
                cin >> running;
            }

            if(running == 2) {
                pracaSoSuborom();
            }

            if(running == 1 && dataz->stop == 1) {
                dataz->casovanie = false;

                break;
            }

            if(running = 1) {
                stahuje  = 1;
                dataz->casovanie = false;
                break;
            }

            if(running == 0) {
                return 0;
            }
        }

        while(stahuje) {
            cout << "Možnosti počas sťahovania: " << endl;
            cout << "   2 pre zastavenie sťahovania" << endl;
            cout << "   3 pre pozastavenie sťahovania" << endl;
            cout << "   4 pre resume sťahovania" << endl;
            cout << "   0 pre exit" << endl;
            cin >> running;

            while(cin.fail()) {
                cout << "Zadajte prosim cislo" << endl;
                cin.clear();
                cin.ignore(256,'\n');
                cin >> running;
            }

            if (running == 2) {
                dataz->stop = 1;
                stahuje = 0;
                break;
            }

            if (running == 3) {
                cout << "Pozastavenie sťahovania" << endl;
                dataz->paused = 1;
            }

            if (running == 4) {
                cout << "Pokračovanie v sťahovaní!" << endl;
                pthread_cond_signal(dataz->resume);
                dataz->paused = 0;
            }

            if(running == 1 & dataz->stop == 1) {
                stahuje = 0;
                running = 10;
            }

            if(running == 0) {
                return 0;
            }
        }
        pthread_join(download, NULL);
        running = 10;
    }
}

int main() {
    pthread_t hlavne;
    pthread_mutex_t mut;
    pthread_cond_t cond1;

    pthread_mutex_init(&mut, nullptr);
    pthread_cond_init(&cond1, nullptr);

    myData dataz(&mut, &cond1);
    pthread_create(&hlavne, nullptr, &hlavneVlakno, (void*)(&dataz));

    pthread_join(hlavne, nullptr);
    pthread_cond_destroy(&cond1);
    pthread_mutex_destroy(&mut);

}

void pracaSoSuborom() {
    string subor;
    int akcia;

    system("ls /home/${USER}/DownloadManager");
    cout << "\n Vyberte subor: \n";
    cin >> subor;

    cout << "Co chcete so suborom urobit? \n";
    cout << "   1 Zmazat\n";
    cout << "   2 Premenovat\n";
    cout << "   3 Kopirovat\n";
    cout << "   4 Premiestni subor\n";

    cin >> akcia;
    while(cin.fail()) {
        cout << "Zadajte prosim cislo" << endl;
        cin.clear();
        cin.ignore(256,'\n');
        cin >> akcia;
    }

    switch (akcia) {
        case 1: {
            string zmazanie;
            zmazanie = "rm /home/${USER}/DownloadManager/" + subor;
            system(zmazanie.c_str());
            cout << "Subor " << subor << " úspešne zmazaný!" <<endl;
            break;
        }

        case 2: {
            string noveMeno;
            string premenovanie;

            cout << "Zadajte nove meno\n";
            cin >> noveMeno;

            premenovanie = "mv /home/${USER}/DownloadManager/" + subor + " /home/${USER}/DownloadManager/" + noveMeno;
            system(premenovanie.c_str());
            cout << "Súbor "<< subor << " premenovaný na: " << noveMeno<<endl;
            break;
        }

        case 3: {
            string skopirujDo;
            string skopiruj;
            cout << "Zadajte adresu kam chcete skopirovat subor: \n";
            cin >> skopirujDo;
            skopiruj = "cp /home/${USER}/DownloadManager/" + subor + " " + skopirujDo;
            system(skopiruj.c_str());

            break;
        }

        case 4: {
            string noveMiesto;
            string presunutie;
            cout << "Zadajte nove miesto\n";
            cin >> noveMiesto;
            presunutie = "mv /home/${USER}/DownloadManager/" + subor + " "+ noveMiesto + "/" + subor;
            system(presunutie.c_str());
            break;
        }
    }

}
